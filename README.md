# GEMS - coding ixD  student software project

The student project GEMS (previous working name "Memory Box") was created and developed by 
Antje-Carolin Goldau and Maximilian Stendler (Computer Science students from Freie Universität Berlin)
and Nitzan Ron and Yi-Ting Chen (product design students from Kunsthochschule Weissensee).

## about the repositories

This GitLab group contains several components we developed and experimented with for our prototype. 
The result is far from a finished product but should at least provide insight to what we envisioned for this project.

### ARtest - AR mobile app

An Unity project with focus on a mobile experience. 
It utilizes a webcam, a gyroscope (or mouse control) and gps (or demo keyboard controls wasd) to see and explore nearby __memstones__. 
It also contains a UI to show a workflow for creating a new memory. 

Optimized for 720x1280 screen resolutions.

### restmembox - rest webserver

A restful spring-boot web application.
The scope of component was to provide an interface to save and retrieve memories and also initiate further operations on the contents. 
It can be setup with a database and also contains a dockerfile to generate a docker image.

### STTTest - speech-to-text with sphinx4

A Java speech-to-text project utilizing sphinx4.

### memcolor

A simple python application to retrieve the 3 main colors from a picture.

### sound-cubes - generate gems from sound

This project was contributed by Nitzan's brother Ohad Ron.
It's a python script to generate 3d meshes from sound files with blender.
