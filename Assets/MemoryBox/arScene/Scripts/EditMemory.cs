﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace MemoryBox.arScene.Scripts
{
	public class EditMemory : MonoBehaviour
	{

		public Text Name;
		public Text CreationDate;
		public Text GpsData;
		public Text Text;
		public GameObject TextContainer;
		public Image Image;
		public GameObject ImageContainer;
		public UserGps UserGps;
		public GameObject ButtonImage;
		public GameObject ButtonText;

		public Text UploadScreenText;
		
		private Memory _memory = new Memory();

		public Memory Memory
		{
			get { return _memory; }
		}

		/// <summary>
		/// Reset the related Canvas and its components. Create an empty memory to edit.
		/// </summary>
		public void NewMemory()
		{
			// creation time
			var timeNow = DateTime.Now;
			var ci = CultureInfo.GetCultureInfo("de-DE");
			var dtfi = ci.DateTimeFormat;
			// gps data
			var gpsData = UserGps.GetLocationInfo();
			
			// memory
			_memory = new Memory
			{
				CreationTime = timeNow,
				GpSdata = gpsData
			};

			// prepare edit Canvas
			Name.text = "Memory #142";
			CreationDate.text = timeNow.ToString(dtfi);
			GpsData.text = gpsData.ToString();
			ImageContainer.SetActive(false);
			ButtonImage.gameObject.SetActive(true);
			Text.text = "";
			TextContainer.SetActive(false);
			ButtonText.gameObject.SetActive(true);
		}

		/// <summary>
		/// Write all needed values from the components into the _memory - and whereever we'll need it later.
		/// </summary>
		public void SaveMemory()
		{
			// name
			_memory.Name = Name.text;
			// note
			_memory.Note = Text.text;
			// image
			
			// Prepare UploadScreen
			UploadScreenText.text = Name.text;
		}
		
	}
}
