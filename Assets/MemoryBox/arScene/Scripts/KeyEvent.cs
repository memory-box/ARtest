﻿using UnityEngine;
using UnityEngine.UI;

namespace MemoryBox.arScene.Scripts
{
	[RequireComponent(typeof(Button))]
	public class KeyEvent : MonoBehaviour
	{

		public Button ButtonToActivate;
		public KeyCode KeyCodeCode;
			
		// Update is called once per frame
		void Update () {
			if (Input.GetKeyDown(KeyCodeCode))
			{
				ButtonToActivate.onClick.Invoke();
			}
		}
	}
}
