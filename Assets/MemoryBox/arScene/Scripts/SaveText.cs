﻿using UnityEngine;
using UnityEngine.UI;

namespace MemoryBox.arScene.Scripts
{
    public class SaveText : MonoBehaviour
    {
        public Text Source;
        public Text Destination;
        public GameObject Container;
        public GameObject Button;

        public void CopyText()
        {
            var text = Source.text;
            Destination.text = text;
            
            // deactivate textbox and enable button or vice versa depending if text is empty
            Container.SetActive(text.Length > 0);
            Button.SetActive(text.Length <= 0);
        }
    }
}