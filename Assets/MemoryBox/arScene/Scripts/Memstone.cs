﻿using System;
using UnityEngine;

namespace MemoryBox.arScene.Scripts
{
	/// <summary>
	/// A MemoryObject gets instantiated by the according factory
	/// </summary>
	[RequireComponent(typeof(Transform))]
	public class Memstone : MonoBehaviour {

		public float Latitude;
		public float Longitude;

		public UserGps UserGps;

		public DebugText Logger;

		/// <value>New location to move to on every update</value>
		private Vector3 _transformLocation;
		
		// Use this for initialization
		private void Start ()
		{
			// if this is placed through the editor we need to add it to the user
			// User is empty if this gets created dynamically
			UserGps.MemoryObjects.Add(this);
		}
	
		// Update is called once per frame
		private void Update () {
			// move relative to player gps
			transform.position = Vector3.MoveTowards(transform.position, _transformLocation, 0.1f);
		}

		/**
		 *	User position is at (0,0). We use the users LocationInfo and its position and this
		 *  objects LocationInfo to determine the position of this object.
		 *  Our visible world is around (-100,-100) - (100,100). 
		 *
		 *	GPS ranges from
		 * 		latitude: -90 (south pole); 90 (north pole)
		 * 		longitude: -180; 180; 0 being the equator
		 *  For easier math we eliminate the negative space by adding 90 and 180 respectively.
		 *	--> so yeah this is a plane. No circulation and probably not working at all around the edge cases.
		 * 	(-0.000000001,-0.000000001) and (0.000000001,0.000000001) won't be nearby in this system.
		 * 	But hey - it's a simplified prototype.
		 *
		 *  source: https://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
		 * 
		 * @param locationInfo the position of the user in the real world
		 * @return position of this object in the world around the user.
		 */
		private Vector3 _positionInWorldSpace(GpSdata gps)
		{
			double latitude = (90 +(double)Latitude) - (90 + (double)gps.Latitude);
			double longitude = (180 + (double)Longitude) - (180 + (double)gps.Longitude);
			
			double z = latitude * 111111;
			double x = 111111 * Math.Cos(latitude * Math.PI / 180) * longitude; // assuming cosin operates in radians
			
			//altitude - y ?
			
			return new Vector3((float)x, 0, (float)z);
		}

		public void UpdatePosition()
		{
			UpdatePosition(UserGps.GetLocationInfo());
		}

		public void UpdatePosition(GpSdata userGps)
		{
			var newPos = _positionInWorldSpace(userGps);
			//Logger.AddDebug(newPos.x+"/"+newPos.y+"/"+newPos.z);
			_transformLocation = newPos;
		}

	}
	
	/// <summary>
	/// Helper class to map the JSON contents.
	/// We can use these instances to generate the Memstones.
	/// </summary>
	[Serializable]
	public class Memory
	{
		// meta data
		public long Id;
		public string Name;
		public DateTime CreationTime;
		public bool Published;

		// gpsData
		public GpSdata GpSdata;
		
		// content
		public byte[] Voice;
		public string VoiceText;
		// enum tone
		// enum weather
		public byte[] Image;
		public string Note;
		public Weather Weather;
		
		// memstone
		public byte[] Mesh;
		public Color Color;
	}
}
