﻿using UnityEngine;
using UnityEngine.UI;

namespace MemoryBox.arScene.Scripts
{
	public class DebugText : Text
	{
		public string Prefix = "--";
		
		public void SetText(string txt)
		{
			text = txt;
		}

		public void AddText(string txt)
		{
			text += txt;
		}

		public void AddDebug(string txt, string prefix = "--")
		{
			txt = prefix+" "+txt;
			Debug.Log(txt);
			AddText(txt+"\n");
		}
	}
}