﻿namespace MemoryBox.arScene.Scripts
{
    public enum Weather
    {
        Rain, Cloud, Snow, Storm, Thunder, Clear
    }
}