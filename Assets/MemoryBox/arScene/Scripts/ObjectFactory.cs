using UnityEngine;

namespace MemoryBox.arScene.Scripts
{
	public class ObjectFactory : MonoBehaviour
	{
		public UserGps UserGps;
		public DebugText Logger;
		public GameObject VisibleAreaContainer;
		
		public Memstone AddMemstone(string name, float latitude, float longitude, Mesh mesh, Material material)
		{
			// todo maybe just use a constructor for this part?
			// then the init would be done with the correct User instance already assigned and goto gps location should be updated in initial step
			
			// main game object
			var memObject = new GameObject(name);
			//memObject.transform.SetParent(VisibleAreaContainer.transform);
			var memComponent = memObject.AddComponent<Memstone>();
			memComponent.Latitude = latitude;
			memComponent.Longitude = longitude;
			memComponent.UserGps = UserGps;
			memComponent.Logger = Logger;
			// todo altitude? maybe use GPSdata as datatype
			
			// child object holding geometry and 3D information
			var memChild = new GameObject("mesh");
			memChild.transform.SetParent(memObject.transform);
			// add mesh filter and assign its mesh
			var meshFilter = memChild.AddComponent<MeshFilter>();
			meshFilter.mesh = mesh;
			// add mesh renderer and assign its material
			var meshRenderer = memChild.AddComponent<MeshRenderer>();
			meshRenderer.material = material;
			
			// todo maybe change some default settings ?

			return memComponent;
		}

		private void Start()
		{
			
		}
	}
}
