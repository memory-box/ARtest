﻿using UnityEngine;

namespace MemoryBox.arScene.Scripts
{
	public class InitDebug : MonoBehaviour {

		// Use this for initialization
		void Start ()
		{
			enabled = Debug.isDebugBuild || Application.isEditor;
		}
	
		// Update is called once per frame
		void Update () {
		
		}
	}
}
