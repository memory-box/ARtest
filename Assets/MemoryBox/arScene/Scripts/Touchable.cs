﻿using UnityEngine;
using UnityEngine.UI;

namespace MemoryBox.arScene.Scripts
{
    // make any part of the gameobject this component belongs to touchable.
    public class Touchable : Graphic
    {
        public override bool Raycast(Vector2 sp, Camera eventCamera)
        {
            //return base.Raycast(sp, eventCamera);
            return true;
        }
 
        protected override void OnPopulateMesh(VertexHelper vh) {
            vh.Clear();
        }
    }
}