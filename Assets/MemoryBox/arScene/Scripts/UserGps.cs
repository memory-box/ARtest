﻿using System.Collections.Generic;
using UnityEngine;

namespace MemoryBox.arScene.Scripts
{
	/**
	 * Holds information about the user and provides actions with this user.
	 *
	 * Currently only GPS and its update mechanism live here.
	 * TODO What else can we add?
	 */
	public class UserGps : MonoBehaviour
	{
		public float Latitude = 52.52629f;
		public float Longitude = 13.40061f;

		private GpSdata _gpSdata;
		private GpSdata GpSdata
		{
			get { return _gpSdata; }
			set 
			{ 
				_gpSdata = value;
				Longitude = _gpSdata.Longitude;
				Latitude = _gpSdata.Latitude;
			}
		}
		
		public Camera MainCamera; // thought was to move the objects relative to the camera orientation
		
		private bool _gpsStarted;
		internal List<Memstone> MemoryObjects = new List<Memstone>(); // list of objects that need gps updates
		
		private readonly LocationService _locationService = new LocationService();
		//private LocationInfo _lastSaneLocation;
		
		// Use this for initialization
		void Start()
		{
			_gpsStarted = StartGps();
			InvokeRepeating("_startGPS", 30f, 30f);
			GpSdata = new GpSdata(Latitude, Longitude, 0); // fake gps @cluster
			UpdateGps(true);
		}

		// Update is called once per frame
		void Update () {
			if (SystemInfo.supportsLocationService)
			{
				UpdateGps(); // todo maybe don't do this every frame - InvokeRepeating maybe? depending on how annoying this is
			}
			else
			{
				// keyboard controls
				var newLocation = GpSdata;
				const float unit = 0.00001f;
				// keyboard movement not tied to camera rotation
				if (Input.GetKey("w"))
				{
					newLocation.Latitude += unit;
				}
				
				if (Input.GetKey("s"))
				{
					newLocation.Latitude -= unit;
				}

				if (Input.GetKey("a"))
				{
					newLocation.Longitude += unit;
				}

				if (Input.GetKey("d"))
				{
					newLocation.Longitude -= unit;
				}
				
				UpdateGps(newLocation);
			}
		}

		private void _startGPS()
		{
			_gpsStarted = StartGps();
		}

		private bool StartGps()
		{
			Input.location.Start(1f,0.1f);
			var maxWait = 20;
			while (!_gpsStarted && Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
			{
				new WaitForSeconds(1);
				maxWait--;
			}
			
			// Service didn't initialize in 20 seconds
			if (maxWait < 1)
			{
				print("Timed out");
			}

			// Connection has failed
			if (Input.location.status == LocationServiceStatus.Failed)
			{
				print("Unable to determine device location");
			}

			return (Input.location.status == LocationServiceStatus.Running);
		}

		public GpSdata GetLocationInfo()
		{
			if (_locationService.status == LocationServiceStatus.Running)
			{
				var locationInfo = _locationService.lastData;
				//Logger.AddDebug(locationInfo.latitude+","+locationInfo.longitude);
				return new GpSdata(locationInfo.latitude, locationInfo.longitude, locationInfo.altitude);
			}
			return this.GpSdata;
		}

		public void UpdateGps(bool force = false, bool notifyObservers = true)
		{
			// set this users gpsdata to current location info
			GpSdata prevLocation = GpSdata;
			GpSdata = GetLocationInfo();
			if (notifyObservers && !prevLocation.Equals(GpSdata) || force)
			{
				NotifyObservers();
			}
			
		}

		public void UpdateGps(GpSdata newLocation)
		{
			GpSdata = newLocation;
			NotifyObservers();
		}

		public void NotifyObservers()
		{
			// notify observers if wanted and location has changed
			foreach (var memoryObject in MemoryObjects)
			{
				memoryObject.UpdatePosition(GpSdata);
			}
		}
	}

	public struct GpSdata
	{
		public float Latitude;
		public float Longitude;
		public float Altitude;

		public GpSdata(float latitude, float longitude, float altitude)
		{
			Latitude = latitude;
			Longitude = longitude;
			Altitude = altitude;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}

			var gpsObj = (GpSdata) obj;
			return gpsObj.Altitude == Altitude && gpsObj.Latitude == Latitude && gpsObj.Longitude == Longitude;
		}

		public override string ToString()
		{
			return Latitude + ", " + Longitude;
		}
	}
}
