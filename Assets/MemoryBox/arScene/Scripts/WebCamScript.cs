﻿using UnityEngine;
using UnityEngine.UI;

namespace MemoryBox.arScene.Scripts
{
	public class WebCamScript : MonoBehaviour
	{

		public GameObject WebCameraPlane;
		public DebugText DebugInfo;
		public DebugText ScreenInfo;
		public DebugText CameraInfo;
		public GameObject InitPanel;
		public GameObject VisibleAreaContainer;
		
		//mouse 
		public float MouseSensitivity = 100.0f;
		public float ClampAngle = 80.0f;
 
		private float _rotY = 0.0f; // rotation around the up/y axis
		private float _rotX = 0.0f; // rIotation around the right/x axis
	
		private bool _camInitialized = false;

		private WebCamTexture _webCameraTexture;
	
		// Use this for initialization
		private void Start ()
		{
			// rotate camera on mobile
			// may need to situational based on rotation ?
			GameObject cameraParent = new GameObject ("camParent");

			DebugInfo.AddDebug("camParent");
		
			cameraParent.transform.position = transform.position;
			transform.parent = cameraParent.transform;
			if (Application.isMobilePlatform) {
				cameraParent.transform.Rotate (Vector3.right, 90);
			} 
		
			DebugInfo.AddDebug("camParent transformed");

			Input.gyro.enabled = true;
		
			DebugInfo.AddDebug("gyro enabled");
		
			// adjust rotation of visible area
			Input.compass.enabled = true;
			VisibleAreaContainer.transform.rotation = Quaternion.Euler(0, -Input.compass.magneticHeading, 0);
			
			_webCameraTexture = new WebCamTexture();
			var camRenderer = WebCameraPlane.GetComponent<Renderer>();
			if (Application.isMobilePlatform)
			{
				camRenderer.material.mainTexture = _webCameraTexture;
				camRenderer.transform.Rotate(Vector3.up, 90);
			}

			_webCameraTexture.filterMode = FilterMode.Trilinear; ////// quality >? performance
		
			// this reports very low values at the initilization
			DebugInfo.AddDebug(_webCameraTexture.width+"x"+_webCameraTexture.height);
		
			// set webCamPanel screen size
			PlaneResize();
		
			DebugInfo.AddDebug("Plane resized");

			if (Application.isMobilePlatform)
			{
				_webCameraTexture.Play();
				DebugInfo.AddDebug("camera plays");
			}

			//InvokeRepeating("RecalculateCameraResolution", 0.5f, 2f);
		}

		public void PlaneResize()
		{
			// this does not seem to work well with gyro activated and eg looking down (at least after initialization)
			var distance = Vector3.Distance(WebCameraPlane.transform.position, transform.position);
			DebugInfo.AddDebug(distance.ToString());
			var v3ViewPort = new Vector3(0, 0, distance);
			var v3BottomLeft = Camera.main.ViewportToWorldPoint(v3ViewPort);
			v3ViewPort.Set(1, 1, distance);
			var v3TopRight = Camera.main.ViewportToWorldPoint(v3ViewPort);
			DebugInfo.AddDebug(v3BottomLeft.ToString());
			DebugInfo.AddDebug(v3TopRight.ToString());
			
			var scaleVector = new Vector3(-1,1,1); //  Desktop scaling - flip upside down camera
			var scaleX = v3BottomLeft.x / 4;
			var scaleZ = v3BottomLeft.y / 4;
			if (Application.isMobilePlatform)
			{
				scaleVector = new Vector3(1,1,1); // mobile flip
				scaleX = v3BottomLeft.y / 4;
				scaleZ = v3BottomLeft.x / 4;
			}
			scaleVector.Scale(new Vector3(scaleX, 1, scaleZ));
			WebCameraPlane.transform.localScale = scaleVector;
		
			ScreenInfo.SetText(Display.main.renderingWidth+"x"+Display.main.renderingHeight);
		
			if(_webCameraTexture != null && _camInitialized) 
				RecalculateCameraResolution();
		}

		public void RecalculateCameraResolution()
		{
			var x = WebCameraPlane.transform.localScale.x;
			var z = WebCameraPlane.transform.localScale.z;
		
			if (x / z > z / x)
			{
				DebugInfo.AddDebug("height=?"+z/x*_webCameraTexture.width);
				_webCameraTexture.requestedHeight = (int)(z / x * _webCameraTexture.width);
			}
			else
			{
				DebugInfo.AddDebug("width=?"+x/z*_webCameraTexture.height);
				_webCameraTexture.requestedWidth = (int)(x / z * _webCameraTexture.height);
			}
			// check whether the camera resolution gets changed at all after our request
			CameraInfo.SetText(_webCameraTexture.width+"x"+_webCameraTexture.height);
		}

		// Update is called once per frame
		private void Update () {
			if (_camInitialized)
			{
				if (SystemInfo.supportsGyroscope)
				{
					// update rotation with gyro
					var cameraRotation = new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y,
						-Input.gyro.attitude.z, -Input.gyro.attitude.w);
					transform.localRotation = cameraRotation;
				}
				else
				{
					Cursor.lockState = Input.GetKey(KeyCode.Escape) ? CursorLockMode.None : CursorLockMode.Locked;
					if (Cursor.lockState == CursorLockMode.Locked)
					{
						var mouseX = Input.GetAxis("Mouse X");
						var mouseY = -Input.GetAxis("Mouse Y");

						_rotY += mouseX * MouseSensitivity * Time.deltaTime;
						_rotX += mouseY * MouseSensitivity * Time.deltaTime;

						_rotX = Mathf.Clamp(_rotX, -ClampAngle, ClampAngle);

						Quaternion localRotation = Quaternion.Euler(_rotX, _rotY, 0.0f);
						transform.rotation = localRotation;
					}
				}
			}
			else if (_webCameraTexture != null &&
			         (_webCameraTexture.width > 100 || _webCameraTexture.height > 100)
			         || WebCamTexture.devices.Length < 1 || !Application.isMobilePlatform)
			{
				// show actual screen content after camera initialisation
				// also resolution stuff
				// eg resizing the plane or request a different camera resolution
				RecalculateCameraResolution();
				_toggleInitPanel();
				_camInitialized = true; // do this all just once
			}
		}

		private void _toggleInitPanel()
		{
			var image = InitPanel.GetComponent<Image>();
			if (image == null) return;
			image.enabled = !image.enabled;
			DebugInfo.AddDebug("init panel toggled");
		}

		public void ToggleWebCam()
		{
			if (_webCameraTexture == null) return; // no nullpointers pls; wait until initialized
			if (_webCameraTexture.isPlaying)
			{
				_webCameraTexture.Pause();
			}
			else
			{
				_webCameraTexture.Play();
			}
		}
	}
}
